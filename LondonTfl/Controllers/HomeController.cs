﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AutoMapper;
using LondonTfl.Models;
using LondonTflCore;

namespace LondonTfl.Controllers
{
    public class HomeController : Controller
    {
        private readonly JsonDataContext _dataContext;
        public HomeController()
        {
            _dataContext = new JsonDataContext();
        }
        // GET: Home
        public ActionResult Types()
        {
            ViewBag.Title = "Types";
            return View(Mapper.Map<IEnumerable<Types>>(_dataContext.GetTypes()));
        }

        public ActionResult StopPoints(string type)
        {
            ViewBag.Title = "StopPoints type: " + type;
            if (type == null) return RedirectToAction("Types");
            return View(Mapper.Map<IEnumerable<TflStopPoint>>(_dataContext.GetStopPoints(type)));
        }

        public ActionResult Arrivals(string id)
        {
            ViewBag.Title = "Arrival on stopPoint: " + id;
            if (id == null) return RedirectToAction("Types");
            return View(Mapper.Map<IEnumerable<TflArrivals>>(_dataContext.GetArrivals(id)));
        }
    }
}