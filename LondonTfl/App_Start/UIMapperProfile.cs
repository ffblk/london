﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AutoMapper;
using LondonTfl.Models;

namespace LondonTfl.App_Start
{
    public class UIMapperProfile : Profile
    {
        protected override void Configure()
        {
            Mapper.CreateMap<LondonTflCore.Models.TflStopPoint, TflStopPoint>();
            Mapper.CreateMap<LondonTflCore.Models.TflArrivals, TflArrivals>();
            Mapper.CreateMap<string, Types>()
                .ForMember(dest => dest.name,
                    opts => opts.MapFrom(src => src));
        }
    }
}