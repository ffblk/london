﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LondonTfl.Models
{
    public class TflStopPoint
    {
        public string type { get; set; }
        public string naptanId { get; set; }
        public string indicator { get; set; }
        public string stopLetter { get; set; }
        public string icsCode { get; set; }
        public string stopType { get; set; }
        public string stationNaptan { get; set; }
        public bool status { get; set; }
        public string id { get; set; }
        public string commonName { get; set; }
        public string placeType { get; set; }
        public object[] children { get; set; }
        public float lat { get; set; }
        public float lon { get; set; }
    }
}