﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LondonTfl.Models
{
    public class TflArrivals
    {
        
        
        
        public string vehicleId { get; set; }
        
        public string stationName { get; set; }
        
		public string linename { get; set; }
		public string platformName { get; set; }
		public string direction { get; set; }
		public string bearing { get; set; }
		
		public string destinationName { get; set; }
		
	
		public string currentLocation { get; set; }
		public string towards { get; set; }
		public string expectedArrival { get; set; }
		
		
    }
}