﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
//using Newtonsoft.Json;

namespace LondonTflCore.Models
{
    public class TflStopPoint
    {
        //[JsonProperty("$type")]
        public string type { get; set; }
        public string naptanId { get; set; }
        public string indicator { get; set; }
        public string stopLetter { get; set; }
        public object[] modes { get; set; }
        public string icsCode { get; set; }
        public string stopType { get; set; }
        public string stationNaptan { get; set; }
        public Lines[] lines { get; set; }
        public LineGroup[] lineGroup { get; set; }
        public LineModeGroups[] lineModeGroups { get; set; }
        public bool status { get; set; }
        public string id { get; set; }
        public string commonName { get; set; }
        public string placeType { get; set; }
        public AdditionalProperties[] additionalProperties { get; set; }
        public object[] children { get; set; }
        public float lat { get; set; }
        public float lon { get; set; }
    }

    public class Lines
    {
        //[JsonProperty("$type")]
        public string type { get; set; }
        public string id { get; set; }
        public string name { get; set; }
        public string uri { get; set; }
    }

    public class LineGroup
    {
        //[JsonProperty("$type")]
        public string type { get; set; }
        public string naptanIdReference { get; set; }
        public string stationAtcoCode { get; set; }
        public string[] lineIdentifier { get; set; }
    }

    public class LineModeGroups
    {
        //[JsonProperty("$type")]
        public string type { get; set; }
        public string modeName { get; set; }
        public string[] lineIdentifier { get; set; }
    }

    public class AdditionalProperties
    {
        //[JsonProperty("$type")]
        public string type { get; set; }
        public string category { get; set; }
        public string key { get; set; }
        public string sourceSystemKey { get; set; }
        public string value { get; set; }
    }
}

