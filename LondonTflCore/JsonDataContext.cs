﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using LondonTflCore.Models;
using Newtonsoft.Json;
using RestSharp;

namespace LondonTflCore
{
    public class JsonDataContext
    {
        private static readonly string _stopPointEndPoint = "https://api.tfl.gov.uk/StopPoint/Type/{0}";
        private static readonly string _arrivalsEndPoint = "https://api.tfl.gov.uk/StopPoint/{0}/Arrivals";
        private static readonly string _stopTypesEndPoint = "https://api.tfl.gov.uk/StopPoint/meta/stoptypes";
        

        public JsonDataContext()
        {
            
        }

        public List<TflStopPoint> GetStopPoints(string type)
        {
            var client = new RestClient(String.Format(_stopPointEndPoint, type));
            var request = new RestRequest("/", Method.GET);
            request.AddHeader("Content-Type", "application/json");
            var response = (RestResponse) client.Execute(request);
            if (response.StatusCode == HttpStatusCode.OK)
            {
                var content = response.Content;
                return JsonConvert.DeserializeObject<List<TflStopPoint>>(content);
            }
            return new List<TflStopPoint>();
        }

        public List<string> GetTypes()
        {
            var client = new RestClient(_stopTypesEndPoint);
            var request = new RestRequest("/", Method.GET);
            request.AddHeader("Content-Type", "application/json");
            var response = (RestResponse)client.Execute(request);
            if (response.StatusCode == HttpStatusCode.OK)
            {
                var content = response.Content;
                return JsonConvert.DeserializeObject<List<string>>(content);
            }
            return new List<string>();
        }
        public List<TflArrivals> GetArrivals (string id)
        {
            var client = new RestClient(String.Format(_arrivalsEndPoint, id));
            var request = new RestRequest("/", Method.GET);
            request.AddHeader("Content-Type", "application/json");
            var response = (RestResponse)client.Execute(request);
            if (response.StatusCode == HttpStatusCode.OK)
            {
                var content = response.Content;
                return JsonConvert.DeserializeObject<List<TflArrivals>>(content);
            }
            return new List<TflArrivals>();
        }
    }
}
